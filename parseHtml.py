import time
import datetime
import codecs
import re
import bs4
from bs4 import BeautifulSoup
from lxml import html
# import filmInfoDownloader

# page = 1
# f = codecs.open(f"./page_{page}.html", 'r', 'utf_8_sig')
# a = f.read()
#
# soup = BeautifulSoup(a, features="lxml")
#
# film_list = soup.find('div', {'class': 'profileFilmsList'})
# tree = html.fromstring(a)
# film_list_lxml = tree.xpath('//div[@class = "profileFilmsList"]')[0]
#
# print(film_list_lxml)



# Beatiful Soup
# movie_link = item.find('div', {'class': 'nameRus'}).find('a').get('href')
# print(movie_link)
# movie_desc = item.find('div', {'class': 'nameRus'}).find('a').text
# print(movie_desc)

# lxml
# movie_link = item_lxml.xpath('.//div[@class = "nameRus"]/a/@href')[0]
# movie_desc = item_lxml.xpath('.//div[@class = "nameRus"]/a/text()')[0]


# print(film_list_lxml)

def read_file(filename):
    f = codecs.open(f"./page_{page}.html", 'r', 'utf_8_sig')
    text = f.read()
    return text

page = 1
def parse_user_datafile_bs(filename):
    results = []
    text = read_file(filename)

    # print(text)

    soup = BeautifulSoup(text, features="lxml")

    # aaa = soup.find('div', {'class', 'rateNowItem rateNowItemAct'}).get('')
    # print(aaa)

    film_list = soup.find('div', {'class': 'profileFilmsList'})
    items = film_list.find_all('div', {'class': ['item', 'item even']})
    for item in items:
        # getting movie_id
        movie_link = item.find('div', {'class': 'nameRus'}).find('a').get('href')
        movie_desc = item.find('div', {'class': 'nameRus'}).find('a').text
        movie_id = re.findall('\d+', movie_link)[0]

        user_rating = item.xpath('.//div[@class = "vote"]/text()')
        # print(user_rating)
        if user_rating:
            user_rating = int(user_rating[0])

        print(user_rating)

        # getting english name
        name_eng = item.find('div', {'class': 'nameEng'}).text

        #getting watch time
        watch_datetime = item.find('div', {'class': 'date'}).text
        date_watched, time_watched = re.match('(\d{2}\.\d{2}\.\d{4}), (\d{2}:\d{2})', watch_datetime).groups()

        # getting user rating
        # ratings = item.find('div', {'class': 'selects vote_widget'})
        # print(ratings)
        # print(item.find('div', {'class': 'selects vote_widget'}))
        a = item.find('div', {'class': 'selects vote_widget'})
        # print(a)

        # b = a.find('div').find('div')
        q = a.find('span', {'id': "rating_user_726838"})
        # c = b.find('div', {'class', 'show_vote_726838 myVote'})
        # b = q.find('div', {'class', 'rateNow rateNow726838'})
        # print(q)

        svw = item.find('div', {'class': 'selects vote_widget'}).find('span').find('div')
        # span = svw.find('span', {'id': 'rating_user_726838'})
        # aaa = span.find('div', {'class': 'rateNow rateNow726838'}).get('vote')
        # ddd = aaa.key('vote')
        # print(svw)
        # print(f"VOTE = {user_rating}")
        # if user_rating:
        #     user_rating = int(user_rating)
        #     print(f"VOTE = {user_rating}")

    # results.append({
    #     'movie_id': movie_id,
    #     'name_eng': name_eng,
    #     'date_watched': date_watched,
    #     'time_watched': time_watched,
    #     'user_rating': user_rating,
    #     'movie_desc': movie_desc
    # })
    return results

parse_user_datafile_bs(f"./page_1.html")