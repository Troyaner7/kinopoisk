import requests
import bs4
from bs4 import BeautifulSoup
from lxml import html

s = requests.Session()
s.headers.update({
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36'
    })
s.cookies.update({
    'Cookie' : 'PHPSESSID=bodfv50urqjrot7gqsk7r63f31; yandex_gid=121363; _csrf_csrf_token=669zhec7s7NLMldHr8zOofk223psoPt3EGptbOoGEhA; mda_exp_enabled=1; user-geo-region-id=121363; user-geo-country-id=2; desktop_session_key=ba43b4776bf1de3412ee76b7bd55fae57d4fe758636f6d75c4a721e1d72920131b974074271ca3463e5c54dff5e840931b29e6b45c4a84181c08cd7acee6024a18fdf86f960042652c33d1909febbd44c2c457b30f8728c7266fe735358f585f; desktop_session_key.sig=_tigdzrBuNaZf3Gavaw6-ORPBa8; yandexuid=2826253791571043791; _ym_uid=1571049365375924729; mda=0; yuidss=2826253791571043791; yandex_login=troyaner7; uid=1572944; mustsee_sort_v5=01.10.200.21.31.41.121.131.51.61.71.81.91.101.111; i=OxGqGAn7yi2V5kMg1uw/VGxmsCL47wGxcND9d4FPzFuEnbAovnVE7m9tv6e+jX4BRlVHvypZ302a2o+fhGjWFT4wElg=; _ym_uid=1571049365375924729; _ym_d=1574522865; vote_data_cookie=52430383bd4c4163d770a66fe76bb917; my_perpages=%7B%2278movies%22%3A200%7D; undefinedClosed=1; kpunk=1; oscargame_vote=f8f4765a775371681709c5acbf5d48a0; oscargame_vote.sig=0CbIpD-zV5xmvkXHpe8la3hCYBA; autoFit=1; tc=1; cycada=tD5bwGT5ZgtNGxPoKhPHNUeC9nFkeCeJRzCiQu4Vq7w=; location=1; stars_sort_v5=01.170.10.70.181; hideBlocks=0; last_visit_friend_lenta=2020-02-15+13%3A13%3A53; mykp_button=movies; mobile=no; crookie=9Q4e3sjPnCf5Eq91TeVorsgqHkjgNVUjipbPWcUYLINUqDebHn4SozucT2IvklluZJba73vfo+UUX8A6Xr3ZIG9l7xg=; cmtchd=MTU4MTc3Nzg5Nzg0MQ==; spravka=dD0xNTgxNzc4MjA2O2k9MTg1LjE1NS4xOC4xNzk7dT0xNTgxNzc4MjA2MDE0ODYwMzYzO2g9MzU3MWZmODc5NjVmZTBiOTE0YzY4OTBiMzVhMjk0ZDE=; _ym_isad=1; ya_sess_id=3:1581845888.5.0.1571261555910:V7JsTg:5b.1|42996156.0.2|30:187356.560499.wpQ9EEsOPrncH5ml4r6FaagVszc; ys=udn.cDp0cm95YW5lcjc%3D#c_chck.787035936; mda2_beacon=1581845888301; sso_status=sso.passport.yandex.ru:synchronized; _ym_wasSynced=%7B%22time%22%3A1581845889835%2C%22params%22%3A%7B%22eu%22%3A0%7D%2C%22bkParams%22%3A%7B%7D%7D; yp=1583944813.oyu.2826253791571043791#1581932290.yu.2826253791571043791; ymex=1584437890.oyu.2826253791571043791; user_country=ru; adblock-warning-toast-hide=1; _ym_visorc_22663942=w; _ym_visorc_52332406=w; _ym_visorc_56177992=w; yandex_plus_metrika_cookie=true; _ym_d=1581850759'
})

film_id = "1162527"
film_url = f"https://www.kinopoisk.ru/film/{film_id}/"
response = requests.get(film_url, headers = s.headers, cookies = s.cookies)

# print(response.text)

soup = BeautifulSoup(response.text, features='lxml')

film_data = soup.find('div', {'class': f"film-img-box feature_film_img_box feature_film_img_box_{film_id}"})

# fff = soup.find('div', {'alt': f"Чужак (The Outsider)"})

img_data = film_data.find('a', {'class': 'popupBigImage'})

img_url = img_data.find('img').get('src')
img_name = img_data.find('img').get('alt')

print(img_url)
print(img_name)

res = requests.get(img_url, headers = s.headers, cookies = s.cookies)
with open(f'./movies/{film_id}.png', 'wb') as output_file:
  output_file.write(res.content)

































#HOW TO DOWNLOAD USING FILM_ID
# s = requests.Session()
#
# s.headers.update({
#         'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36'
#     })
# s.cookies.update({
#     'Cookie' : 'PHPSESSID=bodfv50urqjrot7gqsk7r63f31; yandex_gid=121363; _csrf_csrf_token=669zhec7s7NLMldHr8zOofk223psoPt3EGptbOoGEhA; mda_exp_enabled=1; user-geo-region-id=121363; user-geo-country-id=2; desktop_session_key=ba43b4776bf1de3412ee76b7bd55fae57d4fe758636f6d75c4a721e1d72920131b974074271ca3463e5c54dff5e840931b29e6b45c4a84181c08cd7acee6024a18fdf86f960042652c33d1909febbd44c2c457b30f8728c7266fe735358f585f; desktop_session_key.sig=_tigdzrBuNaZf3Gavaw6-ORPBa8; yandexuid=2826253791571043791; _ym_uid=1571049365375924729; mda=0; yuidss=2826253791571043791; yandex_login=troyaner7; uid=1572944; mustsee_sort_v5=01.10.200.21.31.41.121.131.51.61.71.81.91.101.111; i=OxGqGAn7yi2V5kMg1uw/VGxmsCL47wGxcND9d4FPzFuEnbAovnVE7m9tv6e+jX4BRlVHvypZ302a2o+fhGjWFT4wElg=; _ym_uid=1571049365375924729; _ym_d=1574522865; vote_data_cookie=52430383bd4c4163d770a66fe76bb917; my_perpages=%7B%2278movies%22%3A200%7D; undefinedClosed=1; kpunk=1; oscargame_vote=f8f4765a775371681709c5acbf5d48a0; oscargame_vote.sig=0CbIpD-zV5xmvkXHpe8la3hCYBA; autoFit=1; tc=1; cycada=tD5bwGT5ZgtNGxPoKhPHNUeC9nFkeCeJRzCiQu4Vq7w=; location=1; stars_sort_v5=01.170.10.70.181; hideBlocks=0; last_visit_friend_lenta=2020-02-15+13%3A13%3A53; mykp_button=movies; mobile=no; crookie=9Q4e3sjPnCf5Eq91TeVorsgqHkjgNVUjipbPWcUYLINUqDebHn4SozucT2IvklluZJba73vfo+UUX8A6Xr3ZIG9l7xg=; cmtchd=MTU4MTc3Nzg5Nzg0MQ==; spravka=dD0xNTgxNzc4MjA2O2k9MTg1LjE1NS4xOC4xNzk7dT0xNTgxNzc4MjA2MDE0ODYwMzYzO2g9MzU3MWZmODc5NjVmZTBiOTE0YzY4OTBiMzVhMjk0ZDE=; _ym_isad=1; ya_sess_id=3:1581845888.5.0.1571261555910:V7JsTg:5b.1|42996156.0.2|30:187356.560499.wpQ9EEsOPrncH5ml4r6FaagVszc; ys=udn.cDp0cm95YW5lcjc%3D#c_chck.787035936; mda2_beacon=1581845888301; sso_status=sso.passport.yandex.ru:synchronized; _ym_wasSynced=%7B%22time%22%3A1581845889835%2C%22params%22%3A%7B%22eu%22%3A0%7D%2C%22bkParams%22%3A%7B%7D%7D; yp=1583944813.oyu.2826253791571043791#1581932290.yu.2826253791571043791; ymex=1584437890.oyu.2826253791571043791; user_country=ru; adblock-warning-toast-hide=1; _ym_visorc_22663942=w; _ym_visorc_52332406=w; _ym_visorc_56177992=w; yandex_plus_metrika_cookie=true; _ym_d=1581850759'
# })
#
# film_id = "726838"
# film_url = f"https://www.kinopoisk.ru/film/{film_id}/"
# response = requests.get(film_url, headers = s.headers, cookies = s.cookies)
#
# # print(response.text)
#
# soup = BeautifulSoup(response.text, features='lxml')
#
# film_data = soup.find('div', {'class': f"film-img-box feature_film_img_box feature_film_img_box_{film_id}"})
#
# img_data = film_data.find('a', {'class': 'popupBigImage'})
#
# img_url = img_data.find('img').get('src')
# img_name = img_data.find('img').get('alt')
#
# print(img_url)
# print(img_name)
#
# res = requests.get(img_url, headers = s.headers, cookies = s.cookies)
# with open(f'{img_name}_{film_id}.png', 'wb') as output_file:
#   output_file.write(res.content)