import requests
import random
import time
import datetime
import bs4
from bs4 import BeautifulSoup

user_id = 1572944
s = requests.Session()
s.headers.update({
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36'
    })
s.cookies.update({
    'Cookie' : 'profile_button=main; PHPSESSID=bodfv50urqjrot7gqsk7r63f31; yandex_gid=121363; _csrf_csrf_token=669zhec7s7NLMldHr8zOofk223psoPt3EGptbOoGEhA; mda_exp_enabled=1; user-geo-region-id=121363; user-geo-country-id=2; desktop_session_key=ba43b4776bf1de3412ee76b7bd55fae57d4fe758636f6d75c4a721e1d72920131b974074271ca3463e5c54dff5e840931b29e6b45c4a84181c08cd7acee6024a18fdf86f960042652c33d1909febbd44c2c457b30f8728c7266fe735358f585f; desktop_session_key.sig=_tigdzrBuNaZf3Gavaw6-ORPBa8; yandexuid=2826253791571043791; _ym_uid=1571049365375924729; mda=0; yuidss=2826253791571043791; yandex_login=troyaner7; uid=1572944; mustsee_sort_v5=01.10.200.21.31.41.121.131.51.61.71.81.91.101.111; i=OxGqGAn7yi2V5kMg1uw/VGxmsCL47wGxcND9d4FPzFuEnbAovnVE7m9tv6e+jX4BRlVHvypZ302a2o+fhGjWFT4wElg=; _ym_uid=1571049365375924729; _ym_d=1574522865; vote_data_cookie=52430383bd4c4163d770a66fe76bb917; my_perpages=%7B%2278movies%22%3A200%7D; undefinedClosed=1; kpunk=1; oscargame_vote=f8f4765a775371681709c5acbf5d48a0; oscargame_vote.sig=0CbIpD-zV5xmvkXHpe8la3hCYBA; autoFit=1; _ym_wasSynced=%7B%22time%22%3A1581715845652%2C%22params%22%3A%7B%22eu%22%3A0%7D%2C%22bkParams%22%3A%7B%7D%7D; _ym_isad=1; yp=1583944813.oyu.2826253791571043791#1581802246.yu.2826253791571043791; ymex=1584307846.oyu.2826253791571043791; tc=1; cycada=tD5bwGT5ZgtNGxPoKhPHNUeC9nFkeCeJRzCiQu4Vq7w=; location=1; stars_sort_v5=01.170.10.70.181; hideBlocks=0; last_visit_friend_lenta=2020-02-15+13%3A13%3A53; mykp_button=movies; user_country=ru; _ym_visorc_22663942=b; _ym_visorc_52332406=b; _ym_visorc_56177992=b; _ym_visorc_237742=w; ya_sess_id=3:1581775101.5.0.1571261555910:V7JsTg:5b.1|42996156.0.2|30:187336.295534.YhEqSAkOQ6FDP_UCwaimHDmhgzU; ys=udn.cDp0cm95YW5lcjc%3D#c_chck.3839027887; mda2_beacon=1581775101591; sso_status=sso.passport.yandex.ru:synchronized; adblock-warning-toast-hide=1; _ym_visorc_238735=w; _ym_visorc_10630330=w; mobile=no; yandex_plus_metrika_cookie=true; cmtchd=MTU4MTc3Nzg5Nzg0MQ==; crookie=9Q4e3sjPnCf5Eq91TeVorsgqHkjgNVUjipbPWcUYLINUqDebHn4SozucT2IvklluZJba73vfo+UUX8A6Xr3ZIG9l7xg=; spravka=dD0xNTgxNzc4MjA2O2k9MTg1LjE1NS4xOC4xNzk7dT0xNTgxNzc4MjA2MDE0ODYwMzYzO2g9MzU3MWZmODc5NjVmZTBiOTE0YzY4OTBiMzVhMjk0ZDE=; _ym_d=1581778219'
})

# r = requests.get(url, headers = s.headers, cookies = s.cookies)
# with open('test.html', 'wb') as output_file:
#   output_file.write(r.content)
#
# print(r.text)

def load_user_data(user_id, page, session):
    time.sleep(10 * random.random())
    url = f'http://www.kinopoisk.ru/user/{user_id}/votes/list/ord/date/page/{page}/#list'
    request = session.get(url, headers = s.headers, cookies = s.cookies)
    return request.content

def contain_movies_data(content):
    soup = BeautifulSoup(content, features="html.parser")
    film_list = soup.find('div', {'class': 'profileFilmsList'})
    return film_list is not None

# loading files
page = 1
while page<23:
    print(f"Downloading page #{page}...")
    data = load_user_data(user_id, page, s)
    print(contain_movies_data(data))
    # if contain_movies_data(data):
    with open(f'./page_{page}.html', 'wb') as output_file:
        output_file.write(data)
        print(f"Page #{page} is downloaded.")
        page += 1
    # else:
    #     print("END")
    #     break


# user_id = 1572944
# # establishing session
# s = requests.Session()
# s.headers.update({
#         'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0'
#     })
#
# def load_user_data(user_id, page, session):
#     url = f'http://www.kinopoisk.ru/user/{user_id}/votes/list/ord/date/page/%d/#list'
#     request = session.get(url)
#     return request.text
#
# def contain_movies_data(text):
#     soup = BeautifulSoup(text, features="html.parser")
#     film_list = soup.find('div', {'class': 'profileFilmsList'})
#     return film_list is not None
#
# # loading files
# page = 1
# while True:
#     data = load_user_data(user_id, page, s)
#     if contain_movies_data(data):
#         with open('./page_%d.html' % (page), 'w') as output_file:
#             output_file.write(data.encode('cp1251'))
#             page += 1
#     else:
#             break

#DOWNLOAD IMAGE
# urlimg = "https://www.kinopoisk.ru/WV2s89453/a6812f6svc/qGLkKGioQas33z11GMw__XqcVcHFtj6V5KoKTSDjOaJI0s8MyfymxhybZNdAU0rp1vlQII3jFgJvgHVaGaLM_450m3YNr1PQNdl4ZijzH_tI3lrPy9AHu9eVJlZMxDq3TszSPsTgrch4olbQbWtMrI-xX3Dqp3t1Lt9ZfT_R-eazRZFoKZ5N0xrcSloA8zTrT9tux_7yFb3jvDhWXO8RqmD71gXY2r3SZ5d76V1Fa6CXmFhhypB4aDDKeEUk88nAv1yMQgGwXO86yXpbC8g10FLYef3H6A6ZyZ8IUnbRKZlO1M8R9_qx9VSgc_1XfFmyrKtGXcm3S2MBzXBcHPj10Z9BkmY_omHNLf9Bbh3MNvtYyG_98-gvv9-YOmhW7AmGacbBOvnQmOV0iHbrQXVOmKmMbmXukWZiAuF7binu7u-IdZxGHYJ6wxHcUVQo-yz_c_pNyPX6OYHdqBdzXsw8t2nGzhfCxZnTWpt683doYLius1d49p9caxngS0gf-vvGkF6ddySBd9EM4FtKMsQ091TbcvXf9xyC9KEnSkTCFJ5O6P8x7PC_7VGoWNtGZU25h6hKZNacW2g26ndBD8vq_J5WuH0UmljtC9ZaXSHjN-1Q5HrH6_g7ve60IEhm1wencdnrCOr0o8Fwgmz8aGRbl6e4VWLKlGVQIfdUUyzF-NGbRZJOJrxi0Ajqal434D_0R-hj5-XqH53dpRpte8MZv2DA1y7DwIrgVKtl7ntpXJOOhk1E17JMex37e1oc6f7hm3yRdhSxY-sN41tlP84e12vEatfjwBCtz6AGeXzmOqth2vw19eK76FCVQcp6eX2PtYNsbdC4TUk0_kt-LPP-0It-i30tt3nmD9xWVBHJNOx2_Wjq0tMYute0G3pN5jOLRv3SL9XGjvFrnk7KYklLooy-W0D9vVhQE-ZMTBnRy_yfe7hWLpFR0TnfZH0Jxh7BevB80uDWM5zxuAV3SMYTnWDl0Czp-oL1cKFB9n9URq25jX9b5ZtrYyPASEcPy-_Ejn-udRGXYewR4ENpF_Iu1EbzScz_7Qeh8KwhUUDzN6pVz_Qp6fiw0GqgWMtoWUy4jplNXseFRXw_5VpaPdPn-K9Gs30QnHDKKMFpbRXGCfpjxmnA-s07rcKGGV9qzQa0Qe7JJtLRq-l_tmPrSUh1mKOBRlrloGZlEuluXA_w89q_W5REMZJwwwz8RkUM5RL5bOtC8NvLGr70mwVtdegWrU_r8RXv8ZruQoh47H9Wf5SapHV-y4R3ZSXnbVMt9urqv1SEUCmIXcwm7UFCFtcOy2XqftXw1xy80KgoTUr0A6Vd-c8O7d687mWqZs1HcX25o7BsdNihWlc07H5lNPHR26Jmr0sluVnVLv9cbxHtKelh4k_h_dQtie29Dl1WyQCsY_3vOuLUp8l6qFfJfE9YvqO_S1rHml9_OexafD_p2eeUQrtoErZR8DDOf0EXxh7-ddVL19fcPLb9jwxZZeYqlmr1zD7L17viV7V5-GJyeamqgFdZ-K9bdgbnWGA45srpv1WWShSybMAXymlpO9IC4kvaasHdyR-x6J8nWVvgKp9z9OQV6_y55liaR_tnSkuxmoZ2QPaPXFQFxWZfCur56YFgi2A0vVPAHft9eSjHAcp093bU2fQdqvaZBnBs9SqKQsLLFcTShdJqlHn7TlBoqq2IZn3-rVtIMs1rXxHR3MywXoxhPqh51C72Ykst7S_oeuBpzdzMILXJpClkbcAAnGrO4TTL2IzaR5Nb9kBLXLaCpEl96oxXdyXAXkk31dnfoXS8ZSOjR8sywHBmKtoK9lHWec_p2ieK_4wCTn_0N6hI28QvwteqxXq5RvNWUH60pZ9HXMija14h-ndBC83Xy7tehGQRkFvrDOtrfSnSPeFxxHfj0OMQm9SmOU9cwT6gbNjvNNXDh8VDl0bxZnR3soKTYHXplkpXG_paeCvE8NmvQq53Jo1X0yzGaXQwzj_VRMhi09bpB77ciz5ERfc0h1PJ3hjo_Yr1arlj9UFFRJeViGdj7INaYxP7VkY0yMzkklq9UQiTVM0T-ndZHOEw2VbGWOPEyhWx4YwuWmDPMa1QwOEQ8_a-5EuFQuxBSGSggaFeYPiWXFo0zF5tCsnZ2Lt4sFQesHbnEOt0agHrNuBq_0rN-PcRneaFKUp1wSKlVc7FKM7xpdVdo0z6VU5ol5WRTH_OhkNdPuldXgvG7Me1Z4tgPZZdyDvtck0v9i3ee-V24OfWAZvMlQ1kQOImg0361jfD-ZzHVIZxykxLQbGhjHhd-LdjaxHPfWwTyuXmmnCYXSuJfO03_FxOHfgq0kTEVMXh8jygwYsHWHvMPbhV7-w46vSy1XCFUclVfFu8kZBeWPqbZHs9x3pAFND25JdUjVcUkX_ALfxnQSjQKfJnxU7L0vERs8mqN3pNzRyAZM73Fc_Bn9tSlXPMQE59kKqbQ1bOhlhEMs1dQQfk8vybcoZgCbRS4RLhQ3kt0gnWUMBQysPqJYzUrCBXf_UGrXPEyzLz-onKZoN99GFterKkg0V0yJJKYyHDS1kH0tPGiXOdcA-aT_YkzmB9G9Q580fhYuX10SSt0aM7c0XkIqlwy8Qw8OWKwEmCZtJEcW2Kt6F3UdaYZXgy12Z8JuXV0JRSrUomn3PEDMx6ey3OPPVD-G_s59cbmsmnIXB41yigYdTVGtPfpsdrmGHQWHZ6qqiZS2_8u09SPOhmeAn289K-cqxBE5FWzSrAS0oJ8xj7S99e7-XzFazImSNVf8gHpWjI1gXV3LDzXZlX3GpMXImnuGtz1IV3fTXraWERweztnECaQjO6XcIlxGpCMso24Ubhb-PH7RKA9Y8oTErJPrpI_u8R9cGLylufRv5AdXeVo75scey-YnM46lF6HNPp351Zp38og1TgEsZqZwrGFNVw-VHCwvsujNaeAHhmxD2eb-jRFfHikddhs2PRfEpblpmsTkHxu1xFBsg"
# uuu = "https://avatars.mds.yandex.net/get-kinopoisk-image/1629390/8c94371f-4132-49c3-ad1c-4d2ae9687026/360"
# req = requests.get(uuu)
# with open('img.png', 'wb') as output_file:
#   output_file.write(req.content)



# user_id = 1572944
# url = f'http://www.kinopoisk.ru/user/{user_id}/votes/list/ord/date/page/2/#list'# url для второй страницы
# r = requests.get(url, headers = headers)
# with open('test.html', 'wb') as output_file:
#   output_file.write(r.content)






# TO PARSE IMAGE FROM IT:
# filmUrl = "https://www.kinopoisk.ru/film/4466/"
# q = requests.get(filmUrl, headers = s.headers, cookies = s.cookies)
# with open('test.html', 'wb') as output_file:
#   output_file.write(q.content)
#
# print(q.text)
# CHECI IMG IN:
# <div data-metrika="film_card" id="a5e9cm7r8tmny47aiz2t" class="clearfix ticketButtonExperiment">
#     <div id="photoInfoTable" class="clearfix">
#         <div class="movie-info">
#             <div class="movie-info__sidebar">
#                 <div id="photoBlock" class="originalPoster originalPoster_ticket-button-experiment">
#     <div class="film-img-box feature_film_img_box feature_film_img_box_4466">
#         <a class="addFolder" title="Буду смотреть" href="#"></a>
#                 <span class="left_bg"></span>
#         <span class="bottom_bg"></span>
#         <a class="popupBigImage" href="#" onclick="openImgPopup('/images/film_big/4466.jpg'); return false">
#             <img width="205" src="https://www.kinopoisk.ru/WV2s89453/a6812f6svc/qGLkKGioQas33z11GMw__XqcVcHFtj6V5KoKTSDjOaJI0s8MyfymxhybZNdAU0rp1vlQI4vjGB893ngKDdXpzZ1glHMhjlzzN8VCXT3BEfRj2UHt4doFoMylLH9l_DuPUc_DD8T4g_NxnVbpTEtAorCuWmXqkVdAEfleeTjM1PyXfrBCP41z9gX5al0W0RbxV9F0z_PcMYnSpTxUTuEVuFX03zXU4pzGXKRmx2tHWIu3v3pS6YxJVTDBRVsI6tjlilSYbC6xU-M1-GFmF-oL8kDrTdXo-Buq4qEWanbSO5dM4PAV2MWv8VSwbP5ZVWO4qYN5ddqHeFI42H1QNPv4-JRwpn4Tk0TVDs13YhTBAeJW63nVyd4NrdCbNldWzRyFQsv-P_HVj9lRpnr3Tn9mn4G7XlP3hnt-M-pIRi_F78yaUIReCJ149h73QlUOxSrQdN5_x9_gD539gD53Y_wchU3mxSrw46HKXb5M6mBfYaC4pkZdzY9gfwPMflI67Pfnm2G9XQWPZcoU6VhqFs0N8UvjVcr64yaUzbkkeUvVO6t8wOsf1t2q-X-WfedNXki7pbpmXsqMT3U_xW5jK9b5775rjUYqs2L8Ff5LTRXXCc5D0WHF3dw8gcq7BmxY7ASaaPntG8rxi9NLsVPmW3Boq7q8elb0tFhoHNdNbTn7_PyUWL9gEI5hwCX9XU4dzh7CWvFv1_n8D5Dwnht4RvQVg1TfxC_j0K_6QqNA61x0QZWimmdw3Z9kZDPefUE_y_zFl2CNbRG1ZMkUxlVgPO0y7lPBaf32zB2V7IocXWXgFJl_xPIN0tirxGaHQft9aHqQg7NgetytTWED-k1bP_rsx41rtGsLuGb2Gfd7ZxbWL9dR_Fju_tsFgfGJLV1swDO-Ud7SKefBkM9ph1nLS0VDrbSeTX_JtGp5BMh4eQrW-MK-QLdNI49Q9SveY2g8-yPaRcRq69X9I43vhCh9TNYVplPd7hXrxYvwZrtE1kZKdp6QhVVZ-odaXwDDbmMu7unGqGOISxOyeMoMyn1cDO4f2XDaddDhwCKZy6IgSGjhIIx32O4X2eCR8X-GU9tMX0GKooB3R9SYRnoS3lx7JtLI_7VriUAClEHrJs5_aCvAOuxQ1nDw3cwQs_OsCnZZ_zStStfVPsLZhOdEpnLKdX9skqmEVWL3gWt2JthuWDLw2OKSUqhOAp1l1gnmZkswwzXBe-ZR9vzfJq7vnhVTSeY6qHLk6B7z3rnZX6FE1H9zVbeaoHtG5oFceCXXTF4rwNjtgkawVC-ST8cO4Xx5LPE8wEfDeur63QKdwr4qT1zuKLpM_-ox1d6e-0GAfPN9Xmyjg6poY8uzTXM24VVZEPHF36ldvGUri0fVE8x7QwvTOMh293fpy-g_iOSuNnJf5xa-bMvlO875gflwhEfNWFlsrKSEd1jOm0BzEvheQRjN8_u9fotqI65Z5DDifWg8xCz_csFd4drXL7rmqgVddd0ftk_PzDjS0qzkXrVZ8HlOZZO4h0htypJ_eBDkWU4Lw9jskFyNbh6efuAmylF8INgS8FPXV_T50Dqqzao7W3XUBrdn5OwT0Najy2C2XMhLVlWVmZ5GTc2wfFou22tCOMPm2Y12rWEhnnTRMtpCaSPwLd1PwlPJ-8skrOyDDE51wTeBSOTDPezikcVetnXSaE1im4mjTm_KrEtSI9tweR3m1-eKd6d0C4pH3C3oR0MN0iPKUNtW8cbUG5HDlw17X9cfjWLFzDfl6rzCfLt7yVxRTbemo1pOxpNcXxbNVn0Y9cbNunO6fzWVW-o_xUB0KMwI_EDZY-fB6y256L0fT2jjPZhH3sU4w_WB6GG-bdJ-U2qMqIJ4Yfq6WGU_xWplFuHc54JyiEwptWXBJN5DfB_bKO5O9Vre9voGk9O8PHph6xmbbMXSLO71uMZhvF32d1VNgI-rWVTbs2JlEvxKbDHzyPuoYb9RJY1F7CbXWmAd7x3iW8Vc1OHfDr7UtyVMa8wmil3p7xLjxZHoRLh6x0RwWpuIvVxBy4dqZB7CVWANzvXju0eRTyaTetA4-nZPEuMP7GH1Tvfz0DO5xKkAdG7mJYNi4fQZ19Sw1GWhespkR06ysb5IVM2-TVMW6WthGPLcwbZCh2wEuXnBO8lrRRTaM9Vz23LK9_w0sMO5FXp97iCNRtnJHszlpvJrt27MaHBagqOhfkTSuUd2Fdpqbi3t0t6NdqRKL5ZSxz3uRVgP5CLPT_Zt6-f6HqDnlyBZecg4uVXGxBb196_XVod3yUFWbp-Xg0h18o9oUDXocmIkzP3JnkuyVQ6zXtYT7XdWCOgd7m3Ta8_awRO-7asbd2LzIKxvye0b2-WL1HaEbv5bW16DsYZKWfWfRFgyxHV4N87w7YtBjU0NnkTWKOJCfgvIPu933VjM99Ibn92JLXZDyxGNdOTILvbrqcRUgXvMfXdliKyIfkTJoEtSFcVmTDPW_MuAdpBoIL97ywzaR3wr7AnqadxJ18PtBpnKpB9OWeYGh0jD9BXg-p3SWrla73pVa5CqqnhQ24dYXAPdZnoS7O7Km2aWRj2oTeQv3nF6G8key1vzf-zCzAOW0YAlX33iBYhHwfcK4_Cy00Gff_NtbXiymI9mWvScS0gu-EdNFPrz66tcv0MBmmXmNdhHYB7PGtJW-m3q_fsbksuDGGx36xSXVuvUMM_3kMlGnWP0ek1niqSxTHnetkV3LvxoXjL42cuqV4pNJJND6gTpY106wRL1Z_lvzvPNGqzJph9zWO4di1X00jPZw6bIcJFRzlxuaKuErWRH5plMdCHlcGktx_v5nFSqZi-cTO4l4VhkFNsfy1b1TdD04Se6wr8qcmHxPb1s4PIu4vqgzmGze_d3cmytg69cfPOXQXUZ_n17KPX64KFpsV8mvnvsJcRgaDbvKdNo1EjGyO0Eq-qLBn9i1RqrUuT2DfjnmuJEnEfIW3FWv6GfQXnNoX9XIs0" alt="Что гложет Гилберта Грейпа? (What's Eating Gilbert Grape)" itemprop="image" />
#         </a>
#     </div>