from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from os import listdir
import time
import random
from bs4 import BeautifulSoup
import re
import lxml
import requests
from slugify import slugify

s = requests.Session()
s.headers.update({
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36'
    })
s.cookies.update({
    'Cookie' : 'PHPSESSID=bodfv50urqjrot7gqsk7r63f31; yandex_gid=121363; _csrf_csrf_token=669zhec7s7NLMldHr8zOofk223psoPt3EGptbOoGEhA; mda_exp_enabled=1; user-geo-region-id=121363; user-geo-country-id=2; desktop_session_key=ba43b4776bf1de3412ee76b7bd55fae57d4fe758636f6d75c4a721e1d72920131b974074271ca3463e5c54dff5e840931b29e6b45c4a84181c08cd7acee6024a18fdf86f960042652c33d1909febbd44c2c457b30f8728c7266fe735358f585f; desktop_session_key.sig=_tigdzrBuNaZf3Gavaw6-ORPBa8; yandexuid=2826253791571043791; _ym_uid=1571049365375924729; mda=0; yuidss=2826253791571043791; yandex_login=troyaner7; uid=1572944; mustsee_sort_v5=01.10.200.21.31.41.121.131.51.61.71.81.91.101.111; i=OxGqGAn7yi2V5kMg1uw/VGxmsCL47wGxcND9d4FPzFuEnbAovnVE7m9tv6e+jX4BRlVHvypZ302a2o+fhGjWFT4wElg=; _ym_uid=1571049365375924729; _ym_d=1574522865; vote_data_cookie=52430383bd4c4163d770a66fe76bb917; my_perpages=%7B%2278movies%22%3A200%7D; undefinedClosed=1; kpunk=1; oscargame_vote=f8f4765a775371681709c5acbf5d48a0; oscargame_vote.sig=0CbIpD-zV5xmvkXHpe8la3hCYBA; autoFit=1; tc=1; cycada=tD5bwGT5ZgtNGxPoKhPHNUeC9nFkeCeJRzCiQu4Vq7w=; location=1; stars_sort_v5=01.170.10.70.181; hideBlocks=0; last_visit_friend_lenta=2020-02-15+13%3A13%3A53; mykp_button=movies; mobile=no; crookie=9Q4e3sjPnCf5Eq91TeVorsgqHkjgNVUjipbPWcUYLINUqDebHn4SozucT2IvklluZJba73vfo+UUX8A6Xr3ZIG9l7xg=; cmtchd=MTU4MTc3Nzg5Nzg0MQ==; spravka=dD0xNTgxNzc4MjA2O2k9MTg1LjE1NS4xOC4xNzk7dT0xNTgxNzc4MjA2MDE0ODYwMzYzO2g9MzU3MWZmODc5NjVmZTBiOTE0YzY4OTBiMzVhMjk0ZDE=; _ym_isad=1; ya_sess_id=3:1581845888.5.0.1571261555910:V7JsTg:5b.1|42996156.0.2|30:187356.560499.wpQ9EEsOPrncH5ml4r6FaagVszc; ys=udn.cDp0cm95YW5lcjc%3D#c_chck.787035936; mda2_beacon=1581845888301; sso_status=sso.passport.yandex.ru:synchronized; _ym_wasSynced=%7B%22time%22%3A1581845889835%2C%22params%22%3A%7B%22eu%22%3A0%7D%2C%22bkParams%22%3A%7B%7D%7D; yp=1583944813.oyu.2826253791571043791#1581932290.yu.2826253791571043791; ymex=1584437890.oyu.2826253791571043791; user_country=ru; adblock-warning-toast-hide=1; _ym_visorc_22663942=w; _ym_visorc_52332406=w; _ym_visorc_56177992=w; yandex_plus_metrika_cookie=true; _ym_d=1581850759'
})

# driver = webdriver.Firefox(executable_path=r"C:\Users\Troy7\AppData\Local\Programs\Python\Python38\geckodriver.exe")

# page_source = 'H:/123/'
# file:///H:/123/page_1.html

path_to_movie_list = "H:/Code/kpApi/moviesList.txt"
path_to_movie_posters = "H:/Code/kpApi/moviePosters/"

def download_posters(results, page):
    for movie in results:
        driverMovie = webdriver.Chrome(executable_path=r"C:\Users\Troy7\AppData\Local\Programs\Python\Python38\chromedriver.exe")
        movie_id = movie['movie_id']
        name_eng = movie['name_eng']
        name_rus = movie['name_rus']
        data_watched = movie['date_watched']
        vote = movie['vote']

        film_url = f"https://www.kinopoisk.ru/film/{movie_id}/"
        print(f"Opening page: {film_url}")
        time.sleep(1)
        driverMovie.get(film_url)
        time.sleep(1)
        movieHTML = driverMovie.page_source

        soupMovie = BeautifulSoup(movieHTML, features="lxml")

        # getting additional information for movie
        movie_description = soupMovie.find('div', {'itemprop': "description"}).text

        movie_data = soupMovie.find('div', {'class': f"film-img-box feature_film_img_box feature_film_img_box_{movie_id}"})
        img_data = movie_data.find('a', {'class': 'popupBigImage'})
        if(img_data):
            img_url = img_data.find('img').get('src')
        else:
            img_url = "https://media2.wnyc.org/i/1200/627/l/80/1/blackbox.jpeg"
        print(f"Downloading image for movie {name_rus}/{name_eng} with ID: {movie_id}")
        res = requests.get(img_url, headers=s.headers, cookies=s.cookies)
        print("Image is downloaded successfully")

        print(f"Saving image as: {movie_id}.png")
        with open(f'{path_to_movie_posters}{page}/{movie_id}.png', 'wb') as output_file:
            output_file.write(res.content)
            print("Image is saved successfully")

        # write to file moviesList.txt
        print(f"Saving movie {name_rus}/{name_eng} to movieList.txt")
        with open(path_to_movie_list, 'a', encoding='utf-8') as output_file:
            movieLine = f"{movie_id} | {name_eng} | {name_rus} | {data_watched} | {vote} | {movie_description}\n"
            output_file.write(movieLine)
            print(f"Movie {name_rus}/{name_eng} is saved successfully")

        driverMovie.close()
        # sleepRandom = 50 * random.random()
        # allSleep = 10 + sleepRandom
        # print("Sleeping " + str(allSleep) + " seconds\n")
        time.sleep(3)

page = 22
while page <= 22:
    time.sleep(1)
    driver = webdriver.Chrome(executable_path=r"C:\Users\Troy7\AppData\Local\Programs\Python\Python38\chromedriver.exe")
    time.sleep(1)
    vote_url = f"https://www.kinopoisk.ru/user/1572944/votes/list/ord/date/page/{page}/#list"
    driver.get(vote_url)
    time.sleep(1)

    voteHTML = driver.page_source

    soup = BeautifulSoup(voteHTML, features="lxml")

    film_list = soup.find('div', {'class': 'profileFilmsList'})
    items = film_list.find_all('div', {'class': ['item', 'item even']})

    print("Parsing items and appending information to results list ...")
    results = []
    for item in items:
        # getting movie id
        movie_link = item.find('div', {'class': 'nameRus'}).find('a').get('href')
        movie_id = re.findall('\d+', movie_link)[0]
        # print(movie_id)

        # getting russian name
        name_rus = item.find('div', {'class': 'nameRus'}).find('a').text
        # print(name_rus)

        # getting english name
        name_eng = item.find('div', {'class': 'nameEng'}).text
        # print(name_eng)

        # getting watch time
        watch_datetime = item.find('div', {'class': 'date'}).text
        # getting date time
        date_watched, time_watched = re.match('(\d{2}\.\d{2}\.\d{4}), (\d{2}:\d{2})', watch_datetime).groups()
        # print(date_watched)

        # getting user rating
        vote = item.find('div', {'class': 'vote'}).text
        if vote:
            vote = int(vote)

        results.append({
                'movie_id': movie_id,
                'name_eng': name_eng,
                'name_rus': name_rus,
                'date_watched': date_watched,
                'vote': vote
            })
    print("Items parsed and information appended to results list!")
    time.sleep(1)
    driver.close()

    sleepRandom = 10 * random.random()
    allSleep = 10 + sleepRandom
    print("Sleeping " + str(allSleep) + " seconds\n")
    time.sleep(allSleep)

    download_posters(results, page)
    time.sleep(1)
    print(f"====================================\n\n\n\n\n")
    print(f"Page №{page} was loaded successfully")
    page += 1

print(f"Total movies: {len(results)}\n")
